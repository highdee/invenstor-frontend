export default{
    methods: {
        getSubString(max,data){
            return data.length > max ? data.substring(0,max)+'...' : data;
        },
    },
}