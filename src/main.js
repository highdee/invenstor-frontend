import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex'

import Routes from './routes'
import store from './store/index';

import navs from './components/layout/navs'
// import investor_navs from './components/dashboard/layouts/investor-nav'
import footers from './components/layout/footers'
import DashboardLayout from './components/dashboard/layouts/layout.vue'
import Dashboardnavbar from './components/dashboard/layouts/navbar'
import notification from './components/layout/notification'
import loader from './components/layout/loader'
import vSelect from 'vue-select';
import Multiselect from 'vue-multiselect';
import MoneyFormat from 'vue-money-format' 
import Select2 from 'v-select2-component';




Vue.use(Vuex);


Vue.component('navs',navs);
Vue.component('footers',footers);
Vue.component('user-navbar', Dashboardnavbar)
Vue.component('user-layout', DashboardLayout)
Vue.component('notification',notification)
// Vue.component('investor-nav',investor_navs)
Vue.component('loader',loader);
Vue.component('v-select', vSelect)
Vue.component('multiselect', Multiselect)
Vue.component('money-format',MoneyFormat);
Vue.component('Select2', Select2);

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router: Routes, 
  store
}).$mount('#app')
