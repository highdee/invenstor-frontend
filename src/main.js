import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex'

import Routes from './routes'
import store from './store/index';

import navs from './components/layout/navs'
import footers from './components/layout/footers'
import DashboardLayout from './components/dashboard/layouts/layout.vue'
import Dashboardnavbar from './components/dashboard/layouts/navbar'
import notification from './components/layout/notification'
import loader from './components/layout/loader'
import vSelect from 'vue-select'




Vue.use(Vuex);


Vue.component('navs',navs);
Vue.component('footers',footers);
Vue.component('user-navbar', Dashboardnavbar)
Vue.component('user-layout', DashboardLayout)
Vue.component('notification',notification)
Vue.component('loader',loader);
Vue.component('v-select', vSelect)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router: Routes,
  store
}).$mount('#app')
