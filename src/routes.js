import index from './components/public/index'
import register from './components/public/register'
import login from './components/public/login'
import contact from './components/public/contact'
import services from './components/public/services'
import forget from './components/public/forget'
import reset from './components/public/reset_password'
import verify from './components/public/verify-account'

import userLayout from './components/dashboard/layouts/layout';
import dashboard from './components/dashboard/dashboard'
import addInvention from './components/dashboard/add-invention'
import inventions from './components/dashboard/inventions'
import invention from './components/dashboard/invention'

import invenstorLayout from './components/dashboard/layouts/investor-nav';
import investor from './components/dashboard/investor/index'
import inventionList from './components/dashboard/investor/inventions'
import viewInvetion from './components/dashboard/investor/view-invention'

import Router from 'vue-router';
import Vue from 'vue';

Vue.use(Router);

const router=new Router({ 
	mode: 'history',
	routes: [
		{
            path:'/inventor', 
            component: userLayout, 
            children:[
                {path:'', component:dashboard, name:'dashboard',meta:{ AuthRequired:true }},
                {path:'add-invention', component:addInvention, name:'addInvention',meta:{ AuthRequired:true }},
                {path:'inventions', component:inventions, name:'inventions',meta:{ AuthRequired:true }},
                {path:'invention/:id', component:invention, name:'invention',meta:{ AuthRequired:true }},
                {path:'invention/:id/:title', component:invention, name:'invention',meta:{ AuthRequired:true }},
			],
			meta:{
                AuthRequired:true
			},
        },
		{
            path:'/investor',
            component:invenstorLayout,
            children:[
                {path:'', component:investor, name:'investor'},
                {path:'inventions', component:inventionList, name:'inventionList'},
                {path:'invention/:reference/:name', component:viewInvetion, name:'viewInvetion'},
            ],
            meta:{
                AuthRequired:true
			},
        }, 
        {path:'/', component:index, name:'index'},
        {path:'/verify-account', component:verify, name:'verify'},
        {path:'/forget-password', component:forget,name:'forget'},
        {path:'/reset-password/:token',component: reset, name: 'reset'},
		{path:'/login', component:login,name:'login'},
		{path:'/register', component:register, name:'register'},
		{path:'/contact', component:contact, name:'contact'},
		{path:'/services', component:services, name:'services'},
	]
});

export default router;

router.beforeEach((to,from,next)=>{
    if(to.matched.some(route => route.meta.AuthRequired)) { 
        if (localStorage.getItem('invenstor') == null) { 
            next({
                path: '/login',
                name:'login' 
            }) ;
        } 
        else{ 
            next(); 
        }
    }
    next(); 
});