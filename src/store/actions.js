import axios from 'axios';

export default { 
    handleError(context,error){
        if(error.request.status == 422){
            var resp=JSON.parse(error.request.response);
            var err=resp.errors; 
            var msg='';
            for(var item in err){
                msg=err[item][0];
                break; // it picks the first error ; 
            }  
            context.commit('setNotification',{type:2,msg:msg});
            return msg;
        }else if(error.request.status == 303){
            resp=JSON.parse(error.request.response);
            context.commit('setNotification',{type:2,msg:resp.error}); 
        }
        else if(error.request.status == 404){
            resp=JSON.parse(error.request.response);
            msg= "Route/Link not found";
            context.commit('setNotification',{type:2,msg:msg}); 
        }
        else if(error.request.status == 401){
            msg="Oops! Authentication error, Please login again";
            context.commit('setNotification',{type:2,msg:msg});
            context.commit('logout');
            // window.location.href="/login";
        }
        else {
            msg="Oops! server error, Please try again";
            context.commit('setNotification',{type:2,msg:msg});
        }
    },
    post(context , data){ 
        // console.log(data);
        return new Promise((resolve,reject)=>{
            axios.post(context.state.endpoint+data.endpoint , data.details,{
            })
            .then((data)=>{
                console.log('data resolved')
                resolve(data)
            })
            .catch((error)=>{
                context.dispatch('handleError',error);
                reject(error)
            })
        });
    },
    get(context, endpoint){ 
        return new Promise((resolve,reject)=>{
            axios.get(context.state.endpoint+endpoint)
            .then((data)=>{
                // console.log(data)
                resolve(data)
            })
            .catch((error)=>{
                console.log('error', error.request); 
                context.dispatch('handleError',error);
                reject(error)
            })
        });
    },
    getUser(context){
        return new Promise((resolve,reject)=>{
            axios.get(context.state.endpoint+'get-user?token='+context.state.token)
            .then((data)=>{
                resolve(data)
            })
            .catch((error)=>{
                // context.dispatch('checkIfSessionExpired',error);
                reject(error)
            })
        });
    },

    getDashboard(context){
        // console.log(context.state)
        return new Promise((resolve,reject)=>{
            axios.get(context.state.endpoint+'get-dashboard?token='+context.state.token)
            .then((data)=>{
                context.state.user=data.data.user;
                if(data.data.user.userType == 0){
                    context.state.userType = 'inventor';
                }else{
                    context.state.userType = 'investor';
                }

                // console.log( context.state.userType)
                resolve(data)
                
            })
            .catch((error)=>{
                // context.dispatch('checkIfSessionExpired',error);
                reject(error)
            })
        });
    },
}