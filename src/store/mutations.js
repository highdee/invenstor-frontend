export default {
    setNotification(state,data){
        state.notification.type=data.type;
        state.notification.message=data.msg;  
        
        if(data.unset){
            setTimeout(()=>{
                state.notification.type=0;
                state.notification.message='';
            },6000);
        }
    },
    getUser(state){
        var data=localStorage.getItem('invenstor');    
        data=decodeURIComponent(data); 
        data=JSON.parse(data);
        // console.log(data)
        state.user=data;
        state.token=data.token;
        if(data.userType == 0){
            state.userType = 'inventor';
        }else{
            state.userType = 'investor';
        }

        var result=encodeURIComponent(JSON.stringify(data));
        localStorage.setItem('invenstor',result);
    },
    setUser(state,data){ 
        state.user=data;
        state.token=data.token;

        var result=encodeURIComponent(JSON.stringify(data));
        localStorage.setItem('invenstor',result);
    },

    logout(state){
        window.localStorage.removeItem('invenstor')
        state.user={};
        state.token=null;
        window.location.href="/login";
    },
}